

$(document).ready(function () {
    var $ = document;
    var items = [];
    var editStatus = false;
    var editIndex;
    var startTableCode = "<table id='tbl' border=2 margin='auto'><tr><th>id</th><th>first Name</th><th>last Name</th><th>Birth Place</th><th></th></tr>";
    var displayCode = "";
    var addNewRowCode = '<tr><td><input type="text"   id="idname"></input></td><td><input type="text" id="fname"></input></td><td><input type="text" id="lname"></input></td><td><input type="text" id="bplace"></input></td><td><input type="button" onclick="addRow()" value="Add Row" id="addButton"></input></td></tr>';
    var endTableCode = "</table>";
    var _controller = Controller();

    init();

    function init() {
        loadUsers()
    }

    //load all users
    function loadUsers() {
        _controller.getItems().then(function (items) {
            var i = 0;
            displayCode = "";
            items.forEach((item) => {
                displayCode += '<tr><td>' + item.id + '</td><td>' + item.firstname + '</td><td>' + item.lastname + '</td><td>' + item.birthplace + '</td><td><input type="button" value="Edit" class="Edit" onclick="editUser(this.name)"  name="' + i + '"></input><input type="button" onclick="deleteUser(this.name)" value="Delete" class="Delete" name="' + i + '"><input type="button" value="Save" onclick="saveRow(this.name)" class="Save" name="' + i + '"></input></td></tr>';
                i++;
            });
            $.getElementById("container").innerHTML = startTableCode + displayCode + addNewRowCode + endTableCode;

        })
    }
    

    
});


var editStatus = false;
//var editIndex;
var startTableCode = "<table id='tbl' border=2 margin='auto'><tr><th>id</th><th>first Name</th><th>last Name</th><th>Birth Place</th><th></th></tr>";
var displayCode = "";
var addNewRowCode = '<tr><td><input type="text" id="idname"></input></td><td><input type="text" id="fname"></input></td><td><input type="text" id="lname"></input></td><td><input type="text" id="bplace"></input></td><td><input type="button" onclick="addRow()" value="Add Row" id="addButton"></input></td></tr>';
var endTableCode = "</table>";
var _controller = Controller();
//adduser
function addRow() {
    var $ = document;
    var items = [];
    var rowCount = document.getElementById('tbl').rows.length;
    var i = rowCount-2;
   
    // Get a reference to the table
    let tableRef = document.getElementById('tbl');
    // Insert a row at the end of the table
    let newRow = tableRef.insertRow(rowCount - 1);

    // Insert a cell in the row at index 0
    let newCell = newRow.insertCell(0);
    let newCel2 = newRow.insertCell(1);
    let newCel3 = newRow.insertCell(2);
    let newCel4 = newRow.insertCell(3);
    var cell1 = $.getElementById("idname").value;
    var cell2 = $.getElementById("fname").value;
    var cell3 = $.getElementById("lname").value;
    var cell4 = $.getElementById("bplace").value;
    // Append a text node to the cell
    let newText1 = document.createTextNode(cell1);
    let newText2 = document.createTextNode(cell2);
    let newText3 = document.createTextNode(cell3);
    let newText4 = document.createTextNode(cell4);
    var cell_btn = newRow.insertCell(4);
    cell_btn.insertAdjacentHTML(
        "beforeend",
        "<input type='button' value='Edit' class='Edit'  name='" + i + "'></input><input type='button' onclick='deleteUser(this.name)' value='Delete' class='Delete' name='" + i + "'><input type='button' value='Save'onclick='saveRow(this.name)' class='Save' name='" + i + "'>"
    );
    newCell.appendChild(newText1);
    newCel2.appendChild(newText2);
    newCel3.appendChild(newText3);
    newCel4.appendChild(newText4);
   //clear before add
    var firstcell = $.getElementById("idname");
    var thirdcell = $.getElementById("lname");
    var secondcell = $.getElementById("fname");
    var fourthcell = $.getElementById("bplace");
    firstcell.value = ""; thirdcell.value = ""; secondcell.value = ""; fourthcell.value = "";
}
//delete user
function deleteUser(position) {
    var btnDeletes = document.getElementsByClassName("Delete");
    var row = btnDeletes[position].parentNode.parentNode;
       row.parentNode.removeChild(row);
   

}

//edit user
function editUser(position) {
    debugger
    var $ = document;
    var items = [];
    var editIndex;

    var btnEdits = $.getElementsByClassName("Edit");
    Array.from(btnEdits).forEach((x) => {
       // x.addEventListener("click", (e) => {
            if (editStatus) { return; }
            editStatus = true;
            //editIndex = e.target.name;//keep the index for the verification in the Save opreation
    console.log("editIndex", editIndex)
   // editIndex = position;
   
    var idname = btnEdits[position].parentNode.parentNode.firstChild;//id
            
            var idValue = idname.innerHTML;
            var editid = $.createElement("input");
            idname.innerHTML = "";
            editid.value = idValue;
            editid.setAttribute("className", "intput");
            idname.appendChild(editid);

    var fname = btnEdits[position].parentNode.parentNode.firstChild.nextSibling;//fname
            var nameValue = fname.innerHTML;
            var editName = $.createElement("input");
            fname.innerHTML = "";
            editName.value = nameValue;
            editName.setAttribute("className", "intput");
            fname.appendChild(editName);

    var lname = btnEdits[position].parentNode.parentNode.firstChild.nextSibling.nextSibling;//lname
            var lValue = lname.innerHTML;
            var editl = $.createElement("input");
            lname.innerHTML = "";
            editl.value = lValue;
            lname.appendChild(editl);

    var place = btnEdits[position].parentNode.parentNode.firstChild.nextSibling.nextSibling.nextSibling;//place
            var placeValue = place.innerHTML;
            var editplace = $.createElement("input");
            place.innerHTML = "";
            editplace.value = placeValue;
            place.appendChild(editplace);
        //});
    });
}

//save user
function saveRow(position) {
    var $ = document;
    var items = [];


    var btnSaves = $.getElementsByClassName("Save");
    var row = btnSaves[position].parentNode.parentNode;
    if (!editStatus) { return; }//verify the editStatus
    var index = position;
   console.log("row", row)
    var i =position;
    var displayCode = "";
 
    var idname =row.firstChild.firstChild.value;
    console.log("idname",idname)
    var fname = row.firstChild.nextSibling.firstChild.value;
    var lname = row.firstChild.nextSibling.nextSibling.firstChild.value;
    var place = row.firstChild.nextSibling.nextSibling.nextSibling.firstChild.value;
    items[index] = [idname, fname, lname, place];
    var i = position;
   // debugger
        displayCode = "";
    displayCode += '<tr><td>' + idname + '</td><td>' + fname + '</td><td>' + lname + '</td><td>' + place + '</td><td><input type="button" value="Edit" class="Edit" onclick="editUser(this.name)"  name="' + i + '"></input><input type="button" onclick="deleteUser(this.name)" value="Delete" class="Delete" name="' + i + '"><input type="button" value="Save" onclick="saveRow(this.name)" class="Save" name="' + i + '"></input></td></tr>';
    
        editIndex = -1;
}
