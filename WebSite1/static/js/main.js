
var _controller = Controller();

var clients = {};
init();

function init() {
    loadUsers();
}

//load all users
function loadUsers() {
   
    _controller.getItems().then(function (clientobj) {
        render(clientobj);
    })
}

function adduser() {
    clients.id = document.getElementById("idname1").value; clients.fname = document.getElementById("fname1").value;
    clients.lname = document.getElementById("lname1").value; clients.place = document.getElementById("bplace1").value;
    clientobj = JSON.stringify(clients)
    _controller.addRecord().then(function (clientobj) {
        render(clientobj);
    })
}

// delete user
function deleteUSER(id) {


    var btnDeletes = document.getElementsByClassName("Delete");

    var row = btnDeletes[id - 1].parentNode.parentNode;

    $.ajax({

        url: 'https://my-json-server.typicode.com/wallouf/json-fake-server-test/users/' + id,
        type: 'DELETE',
        success: function () {

            row.parentNode.removeChild(row);

        },
        error: function (e) {
            alert("error");
        }

    })
}



//--------------------load all user
function render(clients) {


    var i = 0;
    var startTableCode = "<table id='tbl' border=2 margin='auto'><tr><th>id</th><th>first Name</th><th>last Name</th><th>Birth Place</th><th></th></tr>";
    var displayCode = "";
    $(clients).each(function (index, element) {
        displayCode += '<tr><td>' + element.id + '</td><td>' + element.firstname + '</td><td>' + element.lastname + '</td><td>' + element.birthplace + '</td><td><input type="button" value="Edit" class="Edit" onclick="editUSER(' + element.id + ')" name="' + i + '"></input><input type="button" onclick="deleteUSER(' + element.id + ')" value="Delete" id="btnDEL" class="Delete" name="' + i + '"></td></tr>';

        i++;
    });
    var addNewRowCode = '<tr><td><input type="text" id="idname1"></input></td><td><input type="text" id="fname1"></input></td><td><input type="text" id="lname1"></input></td><td><input type="text" id="bplace1"></input></td><td><input  class="ADD" type="submit" value="+" onclick="ADD()" id="addButton"></input></td></tr>';
    var endTableCode = "</table>";
    document.getElementById("container").innerHTML = startTableCode + displayCode + addNewRowCode + endTableCode;

}



